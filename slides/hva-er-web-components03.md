# Web Components

```html
<div class="menu">
    <puma-soek />
</div>
<puma-noekkelinfo />
<div class="innhold">
    <puma-venstre-sidebar />
    <oppsummering-til-saksbehandler />
    <puma-saksbehandling />
    <puma-hoeyre-sidebar>
        <dokument-haandtering />
        <saksbehandling-kommentarer />
    </puma-hoeyre-sidebar>
</div>
```
