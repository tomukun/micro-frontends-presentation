# Erfaringer
## Routing

* `react-router-dom` gjør ikke jobben alene.
* Ta kontroll over routing selv → 
    * app state
    * Redux loop

Note:
Du ønsker å få kontroll på hvordan dine web components håndterer en route-endring 

-> gjør til en del av din Redux state og loop.
