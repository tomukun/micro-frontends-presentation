# Klok av skade?

## Ikke alltid smart å...
* satse på bare én teknologi.
* bygge kjempestore og kompliserte apps.
* forsømme ditt vedlikeholdsansvar.
* ha få eller ingen UX-ere.

Note:
All teknologi har begrenset levetid. Planlegg for å kunne bytte den ut.

Ta stilling til hva utbytting vil koste av tid og penger.

Som retire.js: "What you require you must also retire"

Forsømme:
* Ikke oppdatere rammeverk
* Ikke gjøre omskrivinger som gjør det lettere å jobbe med kodebasen.

