# Andre rammeverk?

Vurderte også:
* Polymer
* Elm
* Reason

Note:
* Polymer - Nok en markup-variant. Kan kun brukes for WC.
* Elm - Elm dårlig match for å bygge WC nå? Finnes eksperimentelle biblioteker.
* Reason - Fant ingenting der ute, men kan kanskje funke med https://reasonml.github.io/reason-react/?
