```javascript
class PumaSoek extends HTMLElement {
    constructor() {
        super(); // Kall alltid super.
        // Init, sett opp lyttere, opprett shadow dom...
        const shadowRoot = this.attachShadow({mode: 'open'});
        shadowRoot.appendChild(...);
    }
    connectedCallback() {
        // Kalles når satt inn i DOM.
        // Eksekver logikk og rendering her.
    }
    disconnectedCallback() {
        // Kalles når fjernet fra DOM.
        // Rydd opp og fjern lyttere.
    }
    static get observedAttributes() {
        return ['attributt1']; // Definer attributtene du vil lytte på.
    }
    attributeChangedCallback(attrName, oldVal, newVal) {
        // Kalles når et abservert attributt endres.
    }
}
window.customElements.define('puma-soek', PumaSoek);
```

Note:
Enkleste mulige JS-eksempel
Shadow DOM provides a way for an element to own, render, and style a chunk of DOM that's separate from the rest of the page. Heck, you could even hide away an entire app within a single tag.
